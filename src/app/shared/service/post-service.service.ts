import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IPost } from 'src/app/models/posts.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostServiceService {

  constructor( private http: HttpClient) { }

  getPosts() {
    return this.http
      .get<IPost[]>(
        `https://ngrx-counter-post-default-rtdb.firebaseio.com/posts.json`
        )
      .pipe(map((data) => {
        const posts: IPost[] = [];
        for(let key in data) {
          posts.push({...data[key], id: key})
        }
        return posts;
      }))
  }

  addPost(newPost: IPost): Observable<{ name: string }> {
    return this.http.post<{ name: string }>(
      `https://ngrx-counter-post-default-rtdb.firebaseio.com/posts.json`,
      newPost
    )
  }

  updatePost(post: any) {
    const postData = {
      [post.id]: { title: post.title, description: post.description },
    };
    return this.http.patch(
      `https://ngrx-counter-post-default-rtdb.firebaseio.com/posts.json`,
      postData
    );
  }

  deletePost(id: string) {
    return this.http.delete(
      `https://ngrx-counter-post-default-rtdb.firebaseio.com/posts/${id}.json`,
    );
  }
}
