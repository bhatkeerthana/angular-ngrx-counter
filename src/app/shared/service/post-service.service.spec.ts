import { HttpClient, HttpHandler } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { IPost } from 'src/app/models/posts.model';

import { PostServiceService } from './post-service.service';

describe('PostServiceService', () => {
  let service: PostServiceService;
  let httpService: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        HttpClient,
        HttpHandler
      ]
    });
    service = TestBed.inject(PostServiceService);
    httpService = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("should test getPosts", async() => {
    let res = service.getPosts();
    expect(typeof res).toBe('object');
    spyOn(service, 'getPosts').and.callThrough();
    service.getPosts();
    expect(service.getPosts).toHaveBeenCalled();
  })

  it("should test addPost", async() => {
    let newPost:IPost = {
      title:'new title 1',
      description: 'new description 1'
    }

    let res = service.addPost(newPost);

    expect(typeof res).toBe('object');

    spyOn(service,'addPost').and.callThrough();
    service.addPost(newPost);
    expect(service.addPost).toHaveBeenCalled();
  })

  it("should test updatePost", () => {
    let updatedPost:IPost = {
      id:'111',
      title: 'updated title 1',
      description: 'updated description 1'
    }

    let res = service.updatePost(updatedPost);
    expect(typeof res).toBe('object');
    spyOn(service,'updatePost').and.callThrough();
    service.updatePost(updatedPost);
    expect(service.updatePost).toHaveBeenCalled();
  })

  it("should test deletePost", () => {
    const id = '111';
    let res = service.deletePost(id);
    expect(typeof res).toBe('object');

    spyOn(service,'deletePost').and.callThrough();
    service.deletePost(id);

    expect(service.deletePost).toHaveBeenCalled();
  })
});
