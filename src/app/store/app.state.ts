import { AuthReducer } from "../auth/store/auth.reducer";
import { AUTH_STATE_NAME } from "../auth/store/auth.selectors";
import { AuthState } from "../auth/store/auth.state";
import { counterReducer } from "../counter/store/counter.reducer";
import { ICounterState } from "../counter/store/counter.state";
import { postsReducer } from "../posts/state/posts.reducer";
import { IPostsState } from "../posts/state/posts.state";
import { SharedReducer } from "./shared/shared.reducer";
import { SHARED_STATE_NAME } from "./shared/shared.selector";
import { SharedState } from "./shared/shared.state";

export interface IAppState {
    // counter: ICounterState,
    // posts: IPostsState
    [AUTH_STATE_NAME]: AuthState;

    [SHARED_STATE_NAME]: SharedState
}

export const appReducer = {
    // counterReducer : counterReducer,
    // postsReducer : postsReducer
  [AUTH_STATE_NAME]: AuthReducer,

    [SHARED_STATE_NAME]:SharedReducer
}