import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IAppState } from 'src/app/store/app.state';
import { changeChannelName, customIncrement } from '../store/counter.actions';
import { getchannelName } from '../store/counter.selectors';
import { ICounterState } from '../store/counter.state';

@Component({
  selector: 'app-custom-counter-input',
  templateUrl: './custom-counter-input.component.html',
  styleUrls: ['./custom-counter-input.component.scss']
})
export class CustomCounterInputComponent implements OnInit {

  customCounter!:number;
  channelName$!: Observable<string>;
  // constructor(private store : Store<ICounterState>) { }
  constructor(private store : Store<IAppState>) { }

  ngOnInit(): void {
    // console.log('Channel Observable is called');
    // this.channelName$ = this.store.select('channelName');
    this.channelName$ = this.store.select(getchannelName);
  }

  onAdd() {
    this.store.dispatch(customIncrement({count : this.customCounter}))
    // console.log(this.customCounter);
  }

  changeChannelName(){
    this.store.dispatch(changeChannelName());
  }

}
