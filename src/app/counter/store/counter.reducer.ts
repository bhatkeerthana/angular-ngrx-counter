import { Action, createReducer, on } from "@ngrx/store";
import { changeChannelName, customIncrement, decrement, increment, reset } from "./counter.actions";
import { ICounterState, initialState } from "./counter.state";

const _counterReducer = createReducer(
    initialState,
    on(increment, (state) => {
        return {
            ...state,
            counter : state.counter + 1
        }
    }),
    on(decrement, (state) => {
        return {
            ...state,
            counter : state.counter - 1
        }
    }),
    on(reset, (state) => {
        return {
            ...state,
            counter : 0
        }
    }),
    on(customIncrement, (state, action) => {
        // console.log("Action",action);
        console.log(state.counter, typeof state.counter);
        console.log(action.count, typeof action.count);
        return {
            ...state,
            counter: state.counter + action.count
        }
    }),
    on(changeChannelName, (state) => {
        return {
            ...state,
            channelName : 'Modified Channel Name'
        }
    })
)

export function counterReducer(state=initialState, action: Action) {
    return _counterReducer(state, action)
}