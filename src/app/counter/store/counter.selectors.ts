import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ICounterState } from "./counter.state";
export const COUNTER_STATE_NAME = 'counterReducer';


//  counterReducer -> same name as in AppModule
export const getCounterState = createFeatureSelector<ICounterState>(COUNTER_STATE_NAME);

export const getCounter = createSelector(getCounterState,(state) =>{
    return state.counter;
})

export const getchannelName = createSelector(getCounterState,(state) => {
    return state.channelName;
})
