import * as fromSelectors from './counter.selectors'
import { ICounterState } from './counter.state'

describe('Selectors',()=>{
  it('should select counter',()=>{
    const initialState:ICounterState={
      counter:111,
      channelName:'test channel'
    }
    //ngrx selector have projector method that can be used to
    //project my selector on some kind on state
    const result=fromSelectors.getCounter.projector(initialState)
    expect(result).toBe(111)
  })
  it('should select channel Name',()=>{
    const initialState:ICounterState={
      counter:111,
      channelName:'test channel'
    }
    //ngrx selector have projector method that can be used to
    //project my selector on some kind on state
    const result=fromSelectors.getchannelName.projector(initialState)
    expect(result).toBe('test channel')
  })

})
