import { ComponentFixture, TestBed } from '@angular/core/testing'
import {CounterComponent} from './counter.component'
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core'
import {MockStore,provideMockStore} from '@ngrx/store/testing'
import { ICounterState } from '../store/counter.state'
import { decrement, increment, reset } from '../store/counter.actions'
describe('counter component',()=> {
  let component:CounterComponent;
  let fixture:ComponentFixture<CounterComponent>;
  let store:MockStore<ICounterState>;
  const initialState = {
    counter:23,
    channelName:'demo'
  }
  beforeEach(()=> {
    TestBed.configureTestingModule({
      declarations:[CounterComponent],
      providers:[provideMockStore({initialState})],
      schemas:[CUSTOM_ELEMENTS_SCHEMA]
    })
    store=TestBed.inject(MockStore)
    fixture = TestBed.createComponent(CounterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
    jest.spyOn(store,'dispatch').mockImplementation(()=>{})

  })
  it('should initialise counter component',()=> {
    expect(component).toBeDefined()
  })
  it('onIncrement should dispatch INCREMENT',()=>{
        component.onIncrement();
        expect(store.dispatch).toHaveBeenCalledWith(increment())
  })
  it('onDecrement should dispatch DECREMENT',()=>{
    component.onDecrement();
    expect(store.dispatch).toHaveBeenCalledWith(decrement())
})
it('onReset should dispatch RESET',()=>{
  component.onReset();
  expect(store.dispatch).toHaveBeenCalledWith(reset())
})

})
