import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { IAppState } from 'src/app/store/app.state';
import { getCounter } from '../store/counter.selectors';
import { ICounterState } from '../store/counter.state';

@Component({
  selector: 'app-counter-output',
  templateUrl: './counter-output.component.html',
  styleUrls: ['./counter-output.component.scss']
})
export class CounterOutputComponent implements OnInit,OnDestroy {
  // @Input('counterOutput') counter!: number;

  counter!: number;
  // counter$!: Observable<ICounterState>;
  counter$!: Observable<number>;
  // counterSubscription!:Subscription;

  // constructor(private store:Store<{counterReducer: ICounterState}>) { }
  constructor(private store:Store<IAppState>) { }

  ngOnInit(): void {
    // this.counterSubscription = this.store.select('counterReducer').subscribe(data => {
    //   this.counter = data.counter;
    // });
    // console.log('Counter Observable is called');

    // this.counter$ = this.store.select('counterReducer');
    this.counter$ = this.store.select(getCounter);
  }

  ngOnDestroy(){
    // if(this.counterSubscription) {
    //   this.counterSubscription.unsubscribe();
    // }
  }

}
