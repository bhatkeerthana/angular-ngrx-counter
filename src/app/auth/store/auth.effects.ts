import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { autoLogin, autoLogout, loginStart, loginSuccess, signUpStart, signUpSuccess } from "./auth.actions";
import { catchError, exhaustMap, map, mergeMap, tap } from 'rxjs/operators'
import { AuthService } from "src/app/shared/service/auth.service";
import { of, pipe } from "rxjs";
import { setErrorMessage, setLoadingSpinner } from "src/app/store/shared/shared.actions";
import { Store } from "@ngrx/store";
import { IAppState } from "src/app/store/app.state";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";

@Injectable({
    providedIn:'root'
})
export class AuthEffect {
    
    constructor(
        private action$:Actions, 
        private authService:AuthService,
        private store: Store<IAppState>,
        private router:Router) { }

    login$ = createEffect(() => {
        return this.action$.pipe(
            ofType(loginStart),
            exhaustMap((action) => {
            console.log("entered effect");
                return this.authService.login(action.email, action.password)
                    .pipe(map((data) => {
                        this.store.dispatch(setLoadingSpinner({ status: false }));
                        this.store.dispatch(setErrorMessage({ message: '' }));
                        const user = this.authService.formatUser(data);
                        this.authService.setUserInLocalStorage(user);
                        console.log("User",user);
                        return loginSuccess({ user, redirect: true });
                    }),
                    catchError((errResp: HttpErrorResponse) => {
                        this.store.dispatch(setLoadingSpinner({ status: false }));
                        console.log(errResp);
                        const errorMessage = this.authService.getErrorMessage(
                        errResp.error.error.message
                        );
                        return of(setErrorMessage({ message: errorMessage }));
                    }))
            })
        )
    })

    loginRedirect$ = createEffect(
      () => {
        return this.action$.pipe(
          ofType(...[loginSuccess, signUpSuccess]),
          tap((action) => {
            this.store.dispatch(setErrorMessage({ message: '' }));
            if (action.redirect) {
              this.router.navigate(['/']);
            }
          })
        );
      },
      { dispatch: false }
    );

    signUp$ = createEffect(() => {
        return this.action$.pipe(
            ofType(signUpStart),
            exhaustMap((action) => {
                return this.authService.signUp(action.email, action.password).pipe(
                    map((data) => {
                        this.store.dispatch(setLoadingSpinner({status:false}))
                        const user = this.authService.formatUser(data);
                        this.authService.setUserInLocalStorage(user);
                        return signUpSuccess({user, redirect:true})
                    }),
                    catchError((errResp) => {
                        this.store.dispatch(setLoadingSpinner({ status: false }));
                        const errorMessage = this.authService.getErrorMessage(
                          errResp.error.error.message
                        );
                        return of(setErrorMessage({ message: errorMessage }));
                    })
                )
            })
        )
    });

    // loginSuccessRedirect$ = createEffect(() => {
    //     return this.action$.pipe(
    //         ofType(loginSuccess),
    //         tap(()=> {
    //             this.router.navigate(['/']);
    //         })
    //     )
    // },{dispatch:false})

    autoLogin$ = createEffect(() => {
        return this.action$.pipe(
          ofType(autoLogin),
          mergeMap((action) => {
            const user = this.authService.getUserFromLocalStorage();
            return of(loginSuccess({ user, redirect: false }));
          })
        );
    });
    
    logout$ = createEffect(
        () => {
          return this.action$.pipe(
            ofType(autoLogout),
            map((action) => {
              this.authService.logout();
              this.router.navigate(['auth']);
            })
          );
        },
        { dispatch: false }
    );
}