import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { loginStart } from '../store/auth.actions';

import { LoginComponent } from './login.component';
import { By } from '@angular/platform-browser';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let formBuilder: FormBuilder = new FormBuilder();
  let store: MockStore;
  const initialState = {
    user: null,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports:[ ReactiveFormsModule, FormsModule ],
      providers:[
        provideMockStore({initialState}),
        { provide: FormBuilder, useValue: formBuilder },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    store = TestBed.inject(MockStore);
    fixture = TestBed.createComponent(LoginComponent);
    formBuilder = TestBed.inject(FormBuilder);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(store, 'dispatch').and.callFake(() => {});
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn(component, 'ngOnInit').and.callThrough();
    component.loginForm = formBuilder.group({
      email : new FormControl('', [Validators.required, Validators.email]),
      password : new FormControl('', [Validators.required])
    })
    component.ngOnInit();
    expect(component.ngOnInit).toHaveBeenCalled();
    // expect(component.ngOnInit).toBeDefined()
  });

  it("should dispatch two methods on login", () => {
    spyOn(component, 'onLogin').and.callThrough();
    component.loginForm.controls.email.setValue('test@gmail.com');
    component.loginForm.controls.password.setValue('wrongPassword');
    component.onLogin();

    // let element = fixture.debugElement.query(By.css('.text-danger'));
    // console.log(element);
    expect(store.dispatch).toHaveBeenCalledTimes(2);
    expect(component.onLogin).toHaveBeenCalled();
  })
});
