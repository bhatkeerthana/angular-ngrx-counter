import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { AUTH_STATE_NAME } from './store/auth.selectors';
import { AuthReducer } from './store/auth.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffect } from './store/auth.effects';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes:Routes = [
  { 
    path: '', 
    children:[
      { path:'', redirectTo:'login' },
      { path:'login', component:  LoginComponent },
      { path:'signup', component:SignUpComponent }
    ]
  }
]

@NgModule({
  declarations: [
    LoginComponent,
    SignUpComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    EffectsModule.forFeature([AuthEffect]),
    // StoreModule.forFeature(AUTH_STATE_NAME, AuthReducer)
  ]
})
export class AuthModule { }
