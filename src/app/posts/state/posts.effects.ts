import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, mergeMap, switchMap } from "rxjs/operators";
import { PostServiceService } from "src/app/shared/service/post-service.service";
import { addPost, addPostSuccess, deletePost, deletePostSuccess, loadPosts, loadPostsSuccess, updatePost, updatePostSuccess } from "./posts.actions";

@Injectable()
export class PostsEffects {
    constructor(private action$:Actions,private postsService:PostServiceService){}

    loadPosts$ = createEffect(() => {
        return this.action$.pipe(
            ofType(loadPosts),
            mergeMap(() => {
                return this.postsService.getPosts().pipe(
                    map((posts) => {
                        // console.log("loaded posts");
                        return loadPostsSuccess({posts: posts});
                    })
                )
            })
        )
    })

    addPost$ = createEffect(() => {
        return this.action$.pipe(
            ofType(addPost),
            mergeMap((action) => {
                return this.postsService.addPost(action.post).pipe(
                    map((data) => {
                      // console.log("Data", action.post);
                        // const post = { ...action.post, id: data.name };
                        const post = { ...action.post};
                        return addPostSuccess({ post: post });
                    })
                )
            })
        )
    })

    updatePost$ = createEffect(() => {
      return this.action$.pipe(
        ofType(updatePost),
        switchMap((action) => {
          return this.postsService.updatePost(action.post).pipe(
            map((data) => {
              return updatePostSuccess({ post: action.post });
            })
          );
        })
      );
    });

    deletePost$ = createEffect(() => {
      return this.action$.pipe(
        ofType(deletePost),
        switchMap((action) => {
          return this.postsService.deletePost(action.id).pipe(
            map((data) => {
              return deletePostSuccess({ id: action.id });
            })
          );
        })
      );
    });
}