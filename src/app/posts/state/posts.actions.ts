import { createAction, props } from "@ngrx/store";
import { IPost } from "src/app/models/posts.model";

export const ADD_POST_ACTION = '[Post List] Add Post';
export const ADD_POST_SUCCESS = '[Post List] Add Post Success';

export const EDIT_POST_ACTION = '[Post List] Edit Post'; 
export const EDIT_POST_SUCCESS = '[Post List] Update Posts Success'

export const DELETE_POST_ACTION = '[Post List] Delete Post';
export const DELETE_POST_SUCCESS = '[Post List] Delete Post Success';

export const LOAD_POSTS = '[Post List] Load Posts';
export const LOAD_POSTS_SUCCESS = '[Post List] Load Posts Success'


export const addPost = createAction(
    ADD_POST_ACTION,
    props<{post: IPost}>()
);

export const addPostSuccess = createAction(
    ADD_POST_SUCCESS,
    props<{post: IPost}>() 
)

export const updatePost = createAction(
    EDIT_POST_ACTION,
    props<{post: IPost}>()
);

export const updatePostSuccess = createAction(
    EDIT_POST_SUCCESS,
    props<{ post: IPost }>()
  );

export const deletePost = createAction(
    DELETE_POST_ACTION,
    props<{id: string}>()
)

export const deletePostSuccess = createAction(
    DELETE_POST_SUCCESS,
    props<{ id: string }>()
  );

export const loadPosts = createAction(LOAD_POSTS);
export const loadPostsSuccess = createAction(
    LOAD_POSTS_SUCCESS,
    props<{posts: IPost[]}>()    
);


    