import * as fromActions from './posts.actions';
import * as fromReducer from './posts.reducer';
import { initialState, IPostsState } from './posts.state'

describe('it should test post reducer creation', () => {
    const initialState:IPostsState = {
        posts:[]
    }

    it("should test addPost", () => {
        let expected = fromReducer._postsReducer(initialState, fromActions.addPost);
        expect(expected).toBeDefined();
    })

    it("should test updatePostSuccess", () => {
        let expected = fromReducer._postsReducer(initialState, fromActions.updatePostSuccess);
        expect(expected).toBeDefined();
    })

    it("should test deletePostSuccess", () => {
        let expected = fromReducer._postsReducer(initialState, fromActions.deletePostSuccess);
        expect(expected).toBeDefined();
    })

    it("should test loadPostsSuccess", () => {
        let expected = fromReducer._postsReducer(initialState, fromActions.loadPostsSuccess);
        expect(expected).toBeDefined();
    })
}) 

describe("should test store state", () => {
    const mockState: IPostsState = {
        posts: [
            {
                id : '1',
                title : 'Sample Title 1',
                description: ' Sample Test Description 1'
            },
            {
                id : '2',
                title : 'Sample Title 2',
                description: ' Sample Test Description 2'
            },
        ],
    }

    it('should return the default state', () => {
        
        const action = {
          type: 'Unknown'
        };
        const state = fromReducer._postsReducer(initialState, action);
  
        expect(state).toBe(initialState);
    });

    it('should update the state in an immutable way', () => {
    
        const action = fromActions.loadPostsSuccess({ posts: mockState.posts });
        const state = fromReducer._postsReducer(initialState, action);
  
        expect(state).toEqual(mockState);
        expect(state).not.toBe(mockState);
      });

    it("should add newPost to store", () => {
        const newPost = {
            id : '3',
            title : 'Sample Title 3',
            description: ' Sample Test Description 3'
        }

        const action = fromActions.addPost({post:newPost});
        const state = fromReducer._postsReducer(mockState, action);

        expect(state.posts.length).toEqual(3);
    })

    it("should test update of a post", () => {
        const updatedPost = {
            id : '2',
            title : 'Updated Sample Title 2',
            description: 'Updated Sample Test Description 2'
        }

        const action = fromActions.updatePostSuccess({post: updatedPost});
        const state = fromReducer._postsReducer(mockState,action);

        const upadatedPostTitle = state.posts.find((post) => {
            if(post.id === '2') {
                return post;
            }
        })?.title;
        expect(upadatedPostTitle).toEqual('Updated Sample Title 2')
    });

    it("should test delete a post", () => {
        const id = '2';
        const action = fromActions.deletePostSuccess({id});
        const state = fromReducer._postsReducer(mockState,action);

        expect(state.posts.length).toEqual(1);
    })


})