import { HttpClientTestingModule } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { MockStore, provideMockStore } from "@ngrx/store/testing";
import { Observable, of } from "rxjs"
import { PostServiceService } from "src/app/shared/service/post-service.service";
import { PostsEffects } from "./posts.effects";
import { IPostsState } from "./posts.state";
import { provideMockActions } from '@ngrx/effects/testing';
import * as postActions from '../state/posts.actions';
import { cold, hot } from "jest-marbles";

describe("Testing Post Effects", () => {
    let actions$ : Observable<any>;
    let effects: PostsEffects;
    let postsServiceMock: PostServiceService;
    let store: MockStore;

    let initialState:IPostsState = { 
        posts:[
            {
                id : '1',
                title : 'Sample Title 1',
                description: ' Sample Test Description 1'
            },
            {
                id : '2',
                title : 'Sample Title 2',
                description: ' Sample Test Description 2'
            },
        ] 
    }


    beforeEach(() => {
        TestBed.configureTestingModule({
            imports:[HttpClientTestingModule],
            providers:[
                provideMockActions(()=>actions$),
                provideMockStore({initialState}),
                PostsEffects,
                PostServiceService
            ]
        });
        effects = TestBed.inject(PostsEffects);
        postsServiceMock = TestBed.inject(PostServiceService);
        store = TestBed.inject(MockStore);
    });

    it("should create posts effect", () => {
        expect(effects).toBeTruthy();
    })

    it("should load all the posts", () => {
        const payload = initialState.posts;
        const action = postActions.loadPosts();
        const completeAction = postActions.loadPostsSuccess(initialState);

        actions$ = hot('-a', { a:action });
        const response = cold('-b|', { b:payload });
        const expected = hot('--c', { c: completeAction });
        postsServiceMock.getPosts = jest.fn(() => response);
        expect(effects.loadPosts$).toBeObservable(expected);
    });

    it("should test add post to state", ()=> {
        let newPost = {
            post: {
                id : '3',
                title : 'Sample Title 13',
                description: ' Sample Test Description 3'
            }
        }
        const payload = of(newPost);
        const action = postActions.addPost(newPost);
        const completeAction = postActions.addPostSuccess(newPost);

        actions$ = hot('-a', { a:action });
        const response = cold('-b|', { b:payload });
        const expected = hot('--c', { c: completeAction });
        postsServiceMock.addPost = jest.fn(()=> response)

        expect(effects.addPost$).toBeObservable(expected);


    });

    it("should test update of any selected post", () => {
        let updatedPost = {
            post: {
                id : '3',
                title : 'Updated Sample Title 3',
                description: 'Updated Sample Test Description 3'
            }
        }
        const payload = of(updatedPost);
        const action = postActions.updatePost(updatedPost);
        const completeAction = postActions.updatePostSuccess(updatedPost);

        actions$ = hot('-a', { a:action });
        const response = cold('-b|', { b:payload });
        const expected = cold('--c', { c:completeAction });
        
        postsServiceMock.updatePost = jest.fn(() => response);

        expect(effects.updatePost$).toBeObservable(expected);
    });


    it("should test delete a post when given id", () => {
        let postId = {
            id:'3'
        };
        const payload = of(postId);
        const action = postActions.deletePost(postId);
        const completeAction = postActions.deletePostSuccess(postId);

        actions$ = hot('-a', { a:action });
        const response = cold('-b|', { b:payload });
        const expected = cold('--c', { c:completeAction });
        
        postsServiceMock.deletePost = jest.fn(() => response);

        expect(effects.deletePost$).toBeObservable(expected);
    });
    

})