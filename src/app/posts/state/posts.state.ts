import { IPost } from "src/app/models/posts.model";

export interface IPostsState {
    posts: IPost[];
}

export const initialState: IPostsState = {
    // posts: [
    //     {
    //         id : 1,
    //         title : 'Sample Title 1',
    //         description: ' Sample Test Description 1'
    //     },
    //     {
    //         id : 2,
    //         title : 'Sample Title 2',
    //         description: ' Sample Test Description 2'
    //     },
    // ],
    posts: []
}