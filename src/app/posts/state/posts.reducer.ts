import { Action, createReducer, on } from "@ngrx/store";
import { addPost, deletePost, deletePostSuccess, loadPostsSuccess, updatePost, updatePostSuccess } from "./posts.actions";
import { initialState } from "./posts.state";

export const _postsReducer = createReducer(initialState,
    on(addPost,(state, action) => {

        let post = action.post;
        // post.id = state.posts.length + 1;
        return {
            ...state,
            posts: [...state.posts, {
                // id: state.posts.length + 1,
                ...post
            }]
        }
    }),
    // on(updatePost, (state, action) => {
    //     const updatePosts = state.posts.map((post)=>
    //         {
    //             return post.id === action.post.id ? action.post:post
    //         })  
    //     return {
    //         ...state,
    //         posts: updatePosts
    //     }       
    // }),
    // on(deletePost,(state, action) => {
    //     const updatePosts = state.posts.filter((post) => {
    //         return post.id !== action.id ? post : null;
    //     })

    //     return {
    //         ...state,
    //         posts: updatePosts
    //     }
    // }) ,

    on(updatePostSuccess, (state, action) => {
        const updatePosts = state.posts.map((post)=>
            {
                return post.id === action.post.id ? action.post:post
            })  
        return {
            ...state,
            posts: updatePosts
        }       
    }),
    on(deletePostSuccess,(state, action) => {
        const updatePosts = state.posts.filter((post) => {
            return post.id !== action.id ? post : null;
        })

        return {
            ...state,
            posts: updatePosts
        }
    }) ,
    on(loadPostsSuccess, (state, action) => {
        return {
            ...state,
            posts: action.posts
        }
    })  
);

export function postsReducer(state=initialState, action: Action) {
    return _postsReducer(state, action);
}