import { createFeatureSelector, createSelector } from "@ngrx/store";
import { IPost } from "src/app/models/posts.model";
import { IPostsState } from "./posts.state";

export const POST_STATE_NAME = 'posts';
//  postsReducer =>  same name as given in appreducer
export const getPostsState = createFeatureSelector<IPostsState>(POST_STATE_NAME);

export const getPosts = createSelector(getPostsState,(state) => {
    return state.posts;
})

export const getPostById = createSelector(getPostsState,(state:any, props:any) => {
    // console.log("entered getPostById");
    // console.log(state.posts.filter((post:any) => post.id === props.id))
    return state.posts.find((post: IPost) => post.id === props.id);
})