import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';
import { deletePost, loadPosts } from '../state/posts.actions';

import { PostslistComponent } from './postslist.component';

describe('PostslistComponent', () => {
  let component: PostslistComponent;
  let fixture: ComponentFixture<PostslistComponent>;
  let store:MockStore;

  let initialState = {
    posts: [
        {
            id : '1',
            title : 'Sample Title 1',
            description: ' Sample Test Description 1'
        },
        {
            id : '2',
            title : 'Sample Title 2',
            description: ' Sample Test Description 2'
        },
    ],
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostslistComponent ],
      providers:[
        provideMockStore({ initialState }),
      ],
      imports:[
        RouterTestingModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostslistComponent);
    store = TestBed.inject(MockStore);
    component = fixture.componentInstance;
    spyOn(store,'select').and.returnValue(of(initialState.posts))
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should test ngOnInit", ()=>{
    spyOn(component,'ngOnInit').and.callThrough();
    component.ngOnInit();
    expect(component.ngOnInit).toHaveBeenCalled();
  })

  it("should test onDeletePost",()=>{
    spyOn(component,'onDeletePost').and.callThrough();
    spyOn(window,'confirm').and.returnValue(true);
    spyOn(store, 'dispatch').and.callFake(() => {});

    component.onDeletePost('1');
    expect(store.dispatch).toHaveBeenCalledWith(deletePost({id:'1'}));
    expect(component.onDeletePost).toHaveBeenCalled();
  });

  it('should update the UI when the store changes', () => {
    store.setState({
      posts: [
        {
            id : '1',
            title : 'Sample Title 1',
            description: ' Sample Test Description 1'
        }
      ]
    });
    store.refreshState();
    fixture.detectChanges();
    expect(fixture.debugElement.queryAll(By.css('#postItems')).length).toBe(1);
  });
});
