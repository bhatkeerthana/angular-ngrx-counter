import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IPost } from 'src/app/models/posts.model';
import { IAppState } from 'src/app/store/app.state';
import { deletePost, loadPosts } from '../state/posts.actions';
import { getPosts } from '../state/posts.selectors';
import { IPostsState } from '../state/posts.state';

@Component({
  selector: 'app-postslist',
  templateUrl: './postslist.component.html',
  styleUrls: ['./postslist.component.scss']
})
export class PostslistComponent implements OnInit {

  // posts!:Observable<IPostsState>
  
  posts!:Observable<any>

  constructor(private store:Store<IAppState>) { }

  ngOnInit(): void {
    this.posts = this.store.select(getPosts);
    this.store.dispatch(loadPosts());
    // this.posts.subscribe((data) => {
    //   console.log(data)
    // })
  }

  onDeletePost(id: string) {
    if(confirm('Are you sure you want to delete?')) {
      this.store.dispatch(deletePost({id}));
    }
  }
}
