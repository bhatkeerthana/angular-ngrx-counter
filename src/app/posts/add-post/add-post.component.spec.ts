import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { title } from 'process';
import { element } from 'protractor';
import { addPost } from '../state/posts.actions';

import { AddPostComponent } from './add-post.component';

describe('AddPostComponent', () => {
  let component: AddPostComponent;
  let fixture: ComponentFixture<AddPostComponent>;
  let formBuilder: FormBuilder = new FormBuilder();

  let store: MockStore;

  let initialState = {
    posts: [
        {
            id : 1,
            title : 'Sample Title 1',
            description: ' Sample Test Description 1'
        },
        {
            id : 2,
            title : 'Sample Title 2',
            description: ' Sample Test Description 2'
        },
    ],
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPostComponent ],
      imports:[
        FormsModule,
        ReactiveFormsModule
      ],
      providers:[ 
        provideMockStore({ initialState }),
        { provide: FormBuilder, useValue: formBuilder },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPostComponent);
    store = TestBed.inject(MockStore);
    formBuilder = TestBed.inject(FormBuilder);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.AddPostForm = new FormGroup({
      title: new FormControl(
        null, 
        [
          Validators.required,
          Validators.minLength(6)
        ]),
      description: new FormControl(
        null,
        [
          Validators.required,
          Validators.minLength(10)
        ])
    })

    spyOn(store, 'dispatch').and.callFake(() => {});
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ngOnInit', () => {
    spyOn(component, 'ngOnInit').and.callThrough();
    component.ngOnInit();
    expect(component.ngOnInit).toHaveBeenCalled();
    // expect(component.ngOnInit).toBeDefined()
  });

  it("should dispatch addPost onAddPost", () => {
    component.AddPostForm.get('title')?.setValue('Testing title');
    component.AddPostForm.get('description')?.setValue('Testing description');
    spyOn(component, 'onAddPost').and.callThrough();
    component.onAddPost();

    var newPost = {
      title : component.AddPostForm.value.title,
      description: component.AddPostForm.value.description
    }
    expect(store.dispatch).toHaveBeenCalledWith(
      addPost({post: newPost})
    );    
  });

  it("should not dispatch addPost onAddPost when ", () => {
    component.AddPostForm.get('title')?.setValue('Test');
    component.AddPostForm.get('description')?.setValue('Test');
    spyOn(component, 'onAddPost').and.callThrough();
    component.onAddPost();
    expect(store.dispatch).not.toHaveBeenCalled();
  });

  // incomplete
  it("should display proper error", ()=> {
    component.AddPostForm.get('title')?.setValue('Test');
    component.AddPostForm.get('description')?.setValue('Test');    
    expect(component.AddPostForm.valid).toBeFalsy();

    let element = fixture.debugElement.query(By.css('.text-danger')).nativeElement;
    console.log(element);
  })
});
