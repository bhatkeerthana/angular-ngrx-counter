import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { IPost } from 'src/app/models/posts.model';
import { IAppState } from 'src/app/store/app.state';
import { addPost } from '../state/posts.actions';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
  AddPostForm!: FormGroup

  constructor(private store: Store<IAppState>) { }

  ngOnInit(): void {
    this.AddPostForm = new FormGroup({
      title: new FormControl(
        null, 
        [
          Validators.required,
          Validators.minLength(6)
        ]),
      description: new FormControl(
        null,
        [
          Validators.required,
          Validators.minLength(20)
        ])
    })
  }

  onAddPost() {
    if(!this.AddPostForm.valid)
    {
      return
    }

    var newPost: IPost = {
      title : this.AddPostForm.value.title,
      description: this.AddPostForm.value.description
    }

    console.log(newPost);

    this.store.dispatch(addPost({post: newPost}));
  }

  // showDescriptionErrors() {
  //   const descriptionForm = this.AddPostForm.get('description');
  //   if (descriptionForm?.touched && !descriptionForm.valid) {
  //     if (descriptionForm.errors?.required) {
  //       return 'Description is required';
  //     }

  //     if (descriptionForm.errors?.minlength) {
  //       return 'Description should be of minimum 10 characters length';
  //     }
  //   }
  //   return null;
  // }

}
