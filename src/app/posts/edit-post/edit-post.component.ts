import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/store/app.state';
import { getPostById } from '../state/posts.selectors';
import { IPost } from 'src/app/models/posts.model';
import { Subscription } from 'rxjs';
import { updatePost } from '../state/posts.actions';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit,OnDestroy {

  UpdatePostForm : FormGroup = new FormGroup({
    title: new FormControl(
      null,
      [
        Validators.required,        
        Validators.minLength(6)
      ]
    ),
    description : new FormControl(
      null,
      [
        Validators.required,
        Validators.minLength(10)
      ]
    )
  });
  postId !: number;
  editPost!:IPost;
  editPostSuscription!: Subscription;

  constructor(
    private route: ActivatedRoute,
    private store: Store<IAppState>,
    private router: Router) { }

  ngOnInit(): void {

    this.route.params.subscribe((param) => {
      // this.postId = param.get('id');
      this.postId = param['id'];
      // console.log(this.postId);

      this.editPostSuscription = 
          this.store.select(getPostById,{id: this.postId})
            .subscribe(async (post) => {
              this.editPost = await post;
              // console.log(post);
              this.UpdatePostForm.get('title')?.setValue(this.editPost.title);
              this.UpdatePostForm.get('description')?.setValue(this.editPost.description)
            })
    });
  }

  onUpdatePost() {
    // console.log(this.UpdatePostForm);
    if(!this.UpdatePostForm.valid) {
      return;
    }

    const title=this.UpdatePostForm.value.title;
    const description=this.UpdatePostForm.value.description;
    
    const post:IPost={
      id:this.editPost.id,
      title,description,
    }
    this.store.dispatch(updatePost({post}));
    this.router.navigate(['posts']);
    
    // this.UpdatePostForm.get('title')?.reset();
    // this.UpdatePostForm.get('description')?.reset();
  }

  ngOnDestroy() {
    this.editPostSuscription.unsubscribe();
  }

}

