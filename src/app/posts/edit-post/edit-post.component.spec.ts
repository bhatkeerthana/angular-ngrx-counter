import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import {  of } from 'rxjs';
import { getPostById } from '../state/posts.selectors';

import { EditPostComponent } from './edit-post.component';
import { cold, hot } from 'jest-marbles';
import { updatePost } from '../state/posts.actions';

class MockRouterServices {
  public events = of(
    new NavigationEnd(
      0,
      'http://localhost:4200/login',
      'http://localhost:4200/login'
    )
  );
}

describe('EditPostComponent', () => {
  let component: EditPostComponent;
  let fixture: ComponentFixture<EditPostComponent>;
  let formBuilder: FormBuilder = new FormBuilder();
  let store: MockStore;
  let route: ActivatedRoute;
  let router: Router;
  let routerSpy = { navigate: jasmine.createSpy('navigate') };
  let initialState = {
    posts: [
        {
            id : 1,
            title : 'Sample Title 1',
            description: ' Sample Test Description 1'
        },
        {
            id : 2,
            title : 'Sample Title 2',
            description: ' Sample Test Description 2'
        },
    ],
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPostComponent ],
      imports:[
        FormsModule,
        ReactiveFormsModule
      ],
      providers:[ 
        provideMockStore({ initialState }),
        { provide: FormBuilder, useValue: formBuilder },
        { provide: ActivatedRoute, 
          useValue: {
            params: of({id: 1})
          } 
        },
        { provide: Router, useValue: routerSpy },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPostComponent);
    store = TestBed.inject(MockStore);
    formBuilder = TestBed.inject(FormBuilder);
    route = TestBed.inject(ActivatedRoute);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.editPost = {
      id:'1',
      title:'',
      description:''
    }

    // console.log(component.editPost.id)

    component.UpdatePostForm = new FormGroup({
      title: new FormControl(
        null,
        [
          Validators.required,        
          Validators.minLength(6)
        ]
      ),
      description : new FormControl(
        null,
        [
          Validators.required,
          Validators.minLength(10)
        ]
      )
    });

    spyOn(store, 'dispatch').and.callFake(() => {});
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should test ngOnInit", () => {   
    // store.select = jest.fn().mockReturnValue(hot('-a',{ a: initialState.posts[0] }));
    spyOn(component, 'ngOnInit').and.callThrough();
    component.ngOnInit();
    // fixture.detectChanges();
    // of(store.select).subscribe(data => {
    //   console.log(data);
    //   console.log(component.UpdatePostForm.get('title')?.value);

    //   expect(data).toBe(initialState.posts[0]);
    //   expect(component.UpdatePostForm.get('title')?.value).toBe(initialState.posts[0].title)
    // })
    // expect(store.select).toHaveBeenCalled();
    expect(component.ngOnInit).toHaveBeenCalled();
  });

  it("should be able to select the post by ID", ()=> {
    store.select = jest.fn().mockReturnValue(cold('r', { r: initialState.posts[0] }));
    spyOn(component, 'ngOnInit').and.callThrough();
    component.ngOnInit();
    expect(store.select).toHaveBeenCalledWith(
      getPostById,{id: initialState.posts[0].id}
    ); 
  })

  it("should update post", () => {
    component.UpdatePostForm.get('title')?.setValue('updated Test');
    component.UpdatePostForm.get('description')?.setValue('updated description')
    spyOn(component, 'onUpdatePost').and.callThrough();
    component.onUpdatePost();
    let post = {
      id: component.editPost.id,
      title: component.UpdatePostForm.value.title,
      description: component.UpdatePostForm.value.description
    }
    expect(store.dispatch).toHaveBeenCalledWith(
      updatePost({post:post})
    )
    // jest.spyOn(router, 'navigate').mockResolvedValue()
    // spyOn(router, 'navigate').and.returnValue(true);
    expect(router.navigate).toHaveBeenCalledWith(
      ['posts']
    );
  })

  it("should not update when form is not valid", () => {
    component.UpdatePostForm.get('title')?.setValue('Test');
    component.UpdatePostForm.get('description')?.setValue('Test')
    spyOn(component, 'onUpdatePost').and.callThrough();
    component.onUpdatePost();
    expect(store.dispatch).not.toHaveBeenCalled();
  })
});
